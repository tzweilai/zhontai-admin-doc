# VSCode 配置

::: tip 建议
在团队开发中，配置和使用代码质量工具如 ESLint 和 Prettier 是至关重要的。这些工具可以帮助团队确保代码的一致性和规范性，从而提高代码的可读性、可维护性和可扩展性。

ESLint 是一个可扩展的 JavaScript 代码检查工具，用于识别和报告代码中的模式，以便发现错误和风格问题。

Prettier 则是一个代码格式化工具，它自动地按照设定的规则调整代码格式，使得整个团队的代码风格保持一致。
:::

### VSCode 下载

官网地址：[https://code.visualstudio.com](https://code.visualstudio.com)

### VSCode 插件安装

1. 点击左侧拓展图标

2. 插件安装

> 安装完插件记得重启 VSCode，让插件生效

复制粘贴以下插件按需安装：
- Auto Close Tag
- Auto Rename Tag
- Autoprefixer
- Chinese (Simplified) Language Pack for Visual Studio Code
- Code Runner
- Color Info
- CSS Peek
- DotENV
- ESLint
- filesize
- Git History
- GitLens — Git supercharged
- HTML Boilerplate
- HTML CSS Support
- Icon Fonts
- Iconify IntelliSense
- JavaScript (ES6) code snippets
- JavaScript Debugger
- Jest
- Less IntelliSense
- Material Icon Theme
- Markdown All in One
- Markdown Preview Enhanced
- package-json-upgrade
- Path Intellisense
- Prettier - Code formatter
- PowerShell
- SCSS Everywhere
- SCSS IntelliSense
- stylelint
- Svg Preview
- Tailwind CSS IntelliSense
- vscode-fileheader
- vscode-icons
- vue-helper
- Vue Language Features (Volar)
- 别名路径跳转

也可复制粘贴以下插件批量搜索：
```
Autoprefixer Auto Close Tag Auto Rename Tag Chinese (Simplified) Language Pack for Visual Studio Code Code Runner Color Info CSS Peek DotENV ESLint filesize Git History GitLens — Git supercharged HTML Boilerplate HTML CSS Support Icon Fonts Iconify IntelliSense JavaScript (ES6) code snippets JavaScript Debugger Jest Less IntelliSense Material Icon Theme Markdown All in One Markdown Preview Enhanced package-json-upgrade Path Intellisense Prettier - Code formatter PowerShell SCSS Everywhere SCSS IntelliSense stylelint Svg Preview Tailwind CSS IntelliSense vscode-fileheader vscode-icons vue-helper Vue Language Features (Volar) 别名路径跳转
```

### VSCode 配置首选项
1. 同时按 Ctrl + shift + p 快捷键

2. 选择【首选项：打开用户设置(JSON)】

3. 复制粘贴以下内容到 settings.json 中

``` json
{
  "[scss]": {
    "editor.defaultFormatter": "esbenp.prettier-vscode"
  },
  "[jsonc]": {
    "editor.defaultFormatter": "esbenp.prettier-vscode"
  },
  "[javascript]": {
    "editor.defaultFormatter": "esbenp.prettier-vscode"
  },
  "[html]": {
    "editor.defaultFormatter": "esbenp.prettier-vscode"
  },
  "[typescript]": {
    "editor.defaultFormatter": "esbenp.prettier-vscode"
  },
  "[json]": {
    "editor.defaultFormatter": "esbenp.prettier-vscode"
  },
  "[vue]": {
    "editor.defaultFormatter": "esbenp.prettier-vscode"
  },
  "[css]": {
    "editor.defaultFormatter": "esbenp.prettier-vscode"
  },
  "eslint.options": {
    "extensions": [".js", ".vue", ".ts", ".tsx"]
  },
  "eslint.validate": [
    "vue",
    "html",
    "javascript",
    "graphql",
    "javascriptreact",
    "json",
    "typescript",
    "typescriptreact",
    "vue-html"
  ],
  "eslint.format.enable": true,
  "editor.codeActionsOnSave": {
    "source.fixAll.eslint": true
  },
  "files.associations": {
    "*.cjson": "jsonc",
    "*.wxss": "css",
    "*.wxs": "javascript",
    "*.nvue": "vue",
    "*.ejs": ""
  },
  "editor.formatOnSave": true,
  "editor.tabSize": 2,
  "editor.formatOnType": true,
  "javascript.format.enable": false,
  "search.followSymlinks": false,
  "backgroundCover.opacity": 0.5,
  "typescript.updateImportsOnFileMove.enabled": "always",
  "[markdown]": {
    "editor.defaultFormatter": "esbenp.prettier-vscode"
  },
  "markdownlint.config": {
    "default": true,
    "no-hard-tabs": false,
    "no-inline-html": false,
    "first-line-heading": false,
    "heading-increment": false,
    "no-bare-urls": false
  },
  "editor.minimap.enabled": false,
  "diffEditor.ignoreTrimWhitespace": false,
  "explorer.confirmDelete": false,
  "editor.quickSuggestions": {
    "strings": true,
    "comments": true,
    "other": true
  },
  "html-css-class-completion.includeGlobPattern": "src/theme/**/*.{css,scss,sass,html}",
  "path-intellisense.mappings": {
    "/@": "${workspaceRoot}/src"
  },
  "markdown-preview-enhanced.previewTheme": "vue.css",
  "chatgpt.lang": "cn",
  "chatgpt.temperature": 0.9,
  "git.confirmSync": false,
  "fileheader.Author": "xiaoxue",
  "fileheader.LastModifiedBy": "xiaoxue",
  "security.workspace.trust.untrustedFiles": "open"
}

```

### 代码智能双提示问题

1. 卸载 或 禁用 `Vetur` 插件
2. 删除 `vs code` 配置代码

``` json
"vetur.format.defaultFormatter.html": "prettyhtml",
"vetur.format.defaultFormatter.js": "prettier",
"vetur.validation.template": false,
"vetur.completion.autoImport": false,
"vetur.validation.style": false,
"vetur.validation.interpolation": false,
"vetur.validation.script": false,
```
