# 简介

### 🦄 项目介绍

> Admin 是前后端分离权限管理系统，前端 UI 基于`vue3`开发，后端 Api 基于`.NET 8.0`开发。支持多租户、接口权限、数据权限、动态 Api、任务调度、OSS 文件上传、滑块拼图验证、国内外主流数据库自由切换和动态高级查询。集成统一认证授权、事件总线、数据验证、分布式雪花Id、分布式缓存、分布式事务、IP 限流、性能分析、集成测试、健康检查、接口文档等。

### 🌱 关于 PR

> 希望您在开发过程中如果遇到 bug 且修复后，能及时提交您的 PR 至`Github master分支`供我们改进项目。在提交 pull request 之前，尽可能详细描述问题以方便审核。非常感谢！（由于 Github 会自动同步到 Gitee 项目，目前只接受`Github` Pull Request）

### 🚀 功能介绍

1. 用户管理：管理用户，支持按部门联动用户，用户禁用/启用、重置密码、设置/取消主管，用户可配置多角色、多部门和上级主管，一键登录指定用户功能。
2. 角色管理：管理角色和角色分组，支持按角色联动用户，设置菜单和数据权限，批量添加和移除员工。
3. 部门管理：管理部门，支持树形列表展示。
4. 权限管理：管理权限分组、菜单、权限点，支持树形列表展示。
5. 租户套餐：管理租户套餐，支持租户套餐的菜单权限设置、批量新增/移除套餐企业。
6. 租户管理：管理租户，新增租户后自动初始化租户部门、默认角色和管理员。支持配置套餐、禁用/启用功能。
7. 字典管理：管理数据字典，支持按字典大类联动字典小类。
8. 任务调度：查看任务和任务日志列表，支持任务启动、执行、暂停功能。
9. 缓存管理：缓存列表查询，支持根据缓存键清除缓存
10. 接口管理：管理接口，支持接口同步功能，主要用于新增权限点时选择接口，支持树形列表展示。
11. 视图管理：管理视图，用于新增菜单时选择视图组件，支持树形列表展示。
12. 文件管理：支持文件列表查询、文件上传到OSS和本地/下载、查看大图、复制文件地址、删除文件功能。
13. 登录日志：登录日志列表查询，记录用户登录成功和失败日志。
14. 操作日志：操作日志列表查询，记录用户操作正常和异常日志。

### 💕 特别感谢

- <a href="https://github.com/dotnetcore/FreeSql" target="_blank">FreeSql</a>
- <a href="https://github.com/2881099/FreeRedis" target="_blank">FreeRedis</a>
- <a href="https://github.com/2881099/FreeSql.Cloud" target="_blank">FreeSql.Cloud</a>
- <a href="https://github.com/2881099/FreeScheduler" target="_blank">FreeScheduler</a>

### ❤️ 鸣谢列表

- <a href="https://github.com/dotnet/core" target="_blank">.Net</a>
- <a href="https://github.com/autofac/Autofac" target="_blank">Autofac</a>
- <a href="https://github.com/MapsterMapper/Mapster" target="_blank">Mapster</a>
- <a href="https://github.com/dotnetcore/CAP" target="_blank">DotNetCore.CAP</a>
- <a href="https://github.com/NLog/NLog" target="_blank">NLog</a>
- <a href="https://github.com/yitter/idgenerator" target="_blank">Yitter.IdGenerator</a>
- <a href="https://github.com/JamesNK/Newtonsoft.Json" target="_blank">Newtonsoft.Json</a>
- <a href="https://github.com/domaindrivendev/Swashbuckle.AspNetCore" target="_blank">Swashbuckle.AspNetCore</a>
- <a href="https://github.com/FluentValidation/FluentValidations" target="_blank">FluentValidation.AspNetCore</a>
- <a href="https://github.com/Xabaril/AspNetCore.Diagnostics.HealthChecks" target="_blank">AspNetCore.Diagnostics.HealthChecks</a>
- <a href="https://github.com/MiniProfiler/dotnet" target="_blank">MiniProfiler</a>
- <a href="https://github.com/IdentityServer/IdentityServer4" target="_blank">IdentityServer4</a>
- <a href="https://github.com/stefanprodan/AspNetCoreRateLimit" target="_blank">AspNetCoreRateLimit</a>
- <a href="https://github.com/oncemi/OnceMi.AspNetCore.OSS" target="_blank">OnceMi.AspNetCore.OSS</a>
- <a href="https://gitee.com/pojianbing/lazy-slide-captcha" target="_blank">Lazy.SlideCaptcha.Core</a>
- <a href="https://github.com/ua-parser/uap-csharp" target="_blank">UAParser</a>
